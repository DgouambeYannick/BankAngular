/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HP
 */
@Entity
public class Role implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long roles_id;
    
    private String name;
//    @ManyToMany(mappedBy = "roles")
//    private Collection<User> users;
    @OneToMany(mappedBy = "role",fetch = FetchType.LAZY)
    private Collection<UserRole> rolesusers;
    
    @OneToMany(mappedBy = "role",fetch = FetchType.LAZY)
    private Collection<RoleDroits> rolesdroits;
//    @ManyToMany
//    @JoinTable(name="role_sousmenu",joinColumns = @JoinColumn(name="roles_id"), inverseJoinColumns = @JoinColumn(name="sousmenus_id"))
//    private Collection<Sousmenu> sousmenus;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public Long getRoles_id() {
        return roles_id;
    }

    public void setRoles_id(Long roles_id) {
        this.roles_id = roles_id;
    }
    @JsonIgnore
    @XmlTransient
    public Collection<UserRole> getRolesusers() {
        return rolesusers;
    }

    public void setRolesusers(Collection<UserRole> rolesusers) {
        this.rolesusers = rolesusers;
    }
   
    @JsonIgnore
    @XmlTransient
    public Collection<RoleDroits> getRolesdroits() {
        return rolesdroits;
    }

    public void setRolesdroits(Collection<RoleDroits> rolesdroits) {
        this.rolesdroits = rolesdroits;
    }

    
    
}
