/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author HP
 */
@Entity
@DiscriminatorValue("CompteEpargne")
public class CompteEpargne extends Compte{
    private double taux;

    public CompteEpargne() {
        super();
    }

    
    public CompteEpargne(double taux, String codeCompte, Date Datecreate, double solde) {
        super(codeCompte, Datecreate, solde);
        this.taux = taux;
    }

    
    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }
    
}
