/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DGOUAMBE
 */
@Entity
@Table(name="role_droits")
public class RoleDroits implements Serializable{
    
    @EmbeddedId
    private RoleDroitsPK roleDroitsPK;
    
    @JoinColumn(name = "roles_id", referencedColumnName = "roles_id", insertable = false, updatable = false)
    @ManyToOne
    private Role role;
    
    @JoinColumn(name = "droits_id", referencedColumnName = "droits_id", insertable = false, updatable = false)
    @ManyToOne
    private Droits droits;

    public RoleDroits() {
    }

    public RoleDroits(Long role_id, Long droits_id) {
        this.roleDroitsPK = new RoleDroitsPK(role_id, droits_id);
    }

    public RoleDroits(RoleDroitsPK roleDroitsPK) {
        this.roleDroitsPK = roleDroitsPK;
    }

    public RoleDroitsPK getRoleDroitsPK() {
        return roleDroitsPK;
    }

    public void setRoleDroitsPK(RoleDroitsPK roleDroitsPK) {
        this.roleDroitsPK = roleDroitsPK;
    }
    @JsonIgnore
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Droits getDroits() {
        return droits;
    }

    public void setDroits(Droits droits) {
        this.droits = droits;
    }
    
    
}
