/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author DGOUAMBE
 */
@Embeddable
public class RoleDroitsPK implements Serializable{
    @NotNull
    @Column(name = "roles_id")
    private Long roles_id;
    @NotNull
    @Column(name = "droits_id")
    private Long droits_id;

    public RoleDroitsPK() {
    }

    public RoleDroitsPK(Long roles_id, Long droits_id) {
        this.roles_id = roles_id;
        this.droits_id = droits_id;
    }

    public Long getRoles_id() {
        return roles_id;
    }

    public void setRoles_id(Long roles_id) {
        this.roles_id = roles_id;
    }

    public Long getDroits_id() {
        return droits_id;
    }

    public void setDroits_id(Long droits_id) {
        this.droits_id = droits_id;
    }
    
    
}
