/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author stage_IT6
 */
@Entity
@Table(name = "user_role")
public class UserRole implements Serializable{
    @EmbeddedId
    private UserRolePK userRolePK;
    
    @JoinColumn(name = "users_id", referencedColumnName = "users_id", insertable = false, updatable = false)
    @ManyToOne
    private User user;
    
    @JoinColumn(name = "roles_id", referencedColumnName = "roles_id", insertable = false, updatable = false)
    @ManyToOne
    private Role role;

    public UserRole() {
    }

    public UserRole(UserRolePK userRolePK) {
        this.userRolePK = userRolePK;
    }
    
    public UserRole(Long user_id, Long role_id) {
        this.userRolePK = new UserRolePK(user_id, role_id);
    }
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public UserRolePK getUserRolePK() {
        return userRolePK;
    }

    public void setUserRolePK(UserRolePK userRolePK) {
        this.userRolePK = userRolePK;
    }
    
}
