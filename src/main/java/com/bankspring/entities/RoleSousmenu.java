///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.entities;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import java.io.Serializable;
//import javax.persistence.EmbeddedId;
//import javax.persistence.Entity;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
///**
// *
// * @author stage_IT6
// */
//@Entity
//@Table(name="role_sousmenu")
//public class RoleSousmenu implements Serializable{
//    @EmbeddedId
//    private RoleSousmenuPK roleSousmenuPK;
//    
//    @JoinColumn(name = "sousmenus_id", referencedColumnName = "sousmenus_id", insertable = false, updatable = false)
//    @ManyToOne
//    private Sousmenu sousmenu;
//    
//    @JoinColumn(name = "roles_id", referencedColumnName = "roles_id", insertable = false, updatable = false)
//    @ManyToOne
//    private Role role;
//
//    public RoleSousmenu() {
//    }
//    
//    public RoleSousmenu(Long role_id, Long sousmenu_id){
//        this.roleSousmenuPK = new RoleSousmenuPK(role_id, sousmenu_id);
//    }
//
//    public RoleSousmenu(RoleSousmenuPK roleSousmenuPK) {
//        this.roleSousmenuPK = roleSousmenuPK;
//    }
//    
//    public RoleSousmenuPK getRoleSousmenuPK() {
//        return roleSousmenuPK;
//    }
//
//    public void setRoleSousmenuPK(RoleSousmenuPK roleSousmenuPK) {
//        this.roleSousmenuPK = roleSousmenuPK;
//    }
//
//    public Sousmenu getSousmenu() {
//        return sousmenu;
//    }
//
//    public void setSousmenu(Sousmenu sousmenu) {
//        this.sousmenu = sousmenu;
//    }
//    @JsonIgnore
//    public Role getRole() {
//        return role;
//    }
//
//    public void setRole(Role role) {
//        this.role = role;
//    }
//    
//    
//}
