/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author HP
 */
@Entity
public class User implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long users_id;
 
    @Size(min = 0, max = 1000)
    private String nomEmploye;
    private Integer enabled;
    private String name;
    
    @NotNull
    private String password;
    private String photouser;
    
    @ManyToOne
    @JoinColumn(name="ID_EMP_SUP")
    private User employeSup;
//    @ManyToMany
//    @JoinTable(name="user_role",joinColumns = @JoinColumn(name="users_id"), inverseJoinColumns = @JoinColumn(name="roles_id"))
//    private Collection<Role> roles;
    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private Collection<UserRole> usersrole;
    
    @OneToMany(mappedBy = "user", fetch=FetchType.LAZY)  //Lazy interdit la recuperation de la collection a lappel de la classe sauf en cas de get
    private Collection<Compte> Compte;
    
//    @Enumerated(EnumType.STRING)
//    @Getter @Setter private com.bankspring.dto.Role role;
    
    public User() {
    }

    public User(String nomEmploye, String name, String password) {
        this.nomEmploye = nomEmploye;
        this.name = name;
        this.password = password;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }
    
    @JsonIgnore
    public User getEmployeSup() {
        return employeSup;
    }

    public void setEmployeSup(User employeSup) {
        this.employeSup = employeSup;
    }
   
    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getenabled() {
        return enabled;
    }

    public void setenabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getPhotouser() {
        return photouser;
    }

    public void setPhotouser(String photouser) {
        this.photouser = photouser;
    }

    public Long getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Long users_id) {
        this.users_id = users_id;
    }
    @JsonIgnore
    @XmlTransient
    public Collection<UserRole> getUsersrole() {
        return usersrole;
    }

    public void setUsersrole(Collection<UserRole> usersrole) {
        this.usersrole = usersrole;
    }
    @JsonIgnore
    @XmlTransient
    public Collection<Compte> getCompte() {
        return Compte;
    }

    public void setCompte(Collection<Compte> Compte) {
        this.Compte = Compte;
    }
    
    
}
 