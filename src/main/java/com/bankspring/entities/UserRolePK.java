/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author stage_IT6
 */
@Embeddable
public class UserRolePK implements Serializable{
    @NotNull
    @Column(name = "users_id")
    private Long users_id;
    
    @NotNull
    @Column(name = "roles_id")
    private Long roles_id;

    public UserRolePK() {
    }

    public UserRolePK(Long users_id, Long roles_id) {
        this.users_id = users_id;
        this.roles_id = roles_id;
    }

    public Long getUsers_id() {
        return users_id;
    }

    public void setUsers_id(Long users_id) {
        this.users_id = users_id;
    }

    public Long getRoles_id() {
        return roles_id;
    }

    public void setRoles_id(Long roles_id) {
        this.roles_id = roles_id;
    }
    
    
}
