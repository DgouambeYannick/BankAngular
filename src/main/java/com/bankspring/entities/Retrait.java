/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author HP
 */
@Entity
@DiscriminatorValue("Retrait")
public class Retrait extends Operation{

    public Retrait() {
        super();
    }

    public Retrait(Date dateOperation, double montant) {
        super(dateOperation, montant);
    }
    
}
