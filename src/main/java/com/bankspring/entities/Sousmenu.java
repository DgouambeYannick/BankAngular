///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.entities;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import java.io.Serializable;
//import java.util.Collection;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToMany;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToMany;
//import javax.xml.bind.annotation.XmlTransient;
//
///**
// *
// * @author DGOUAMBE
// */
//@Entity
//public class Sousmenu implements Serializable{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long sousmenus_id;
//    
//    private String Libelle;
////    @ManyToMany(mappedBy = "sousmenus")
////    private Collection<Role> roles;
//    @OneToMany(mappedBy = "sousmenu",fetch = FetchType.LAZY)
//    private Collection<RoleSousmenu> sousmenusrole;
//    
//    @ManyToOne
//    @JoinColumn(name="Id_menu")
//    private Menu menu;
//
//    public Sousmenu(String Libelle) {
//        this.Libelle = Libelle;
//    }
//
//    public Sousmenu() {
//    }
//
//    public String getLibelle() {
//        return Libelle;
//    }
//
//    public void setLibelle(String Libelle) {
//        this.Libelle = Libelle;
//    }
//
//    public Menu getMenu() {
//        return menu;
//    }
//
//    public void setMenu(Menu menu) {
//        this.menu = menu;
//    }
//
//    public Long getSousmenus_id() {
//        return sousmenus_id;
//    }
//
//    public void setSousmenus_id(Long sousmenus_id) {
//        this.sousmenus_id = sousmenus_id;
//    }
//    
//    @JsonIgnore
//    @XmlTransient
//    public Collection<RoleSousmenu> getSousmenusrole() {
//        return sousmenusrole;
//    }
//
//    public void setSousmenusrole(Collection<RoleSousmenu> sousmenusrole) {
//        this.sousmenusrole = sousmenusrole;
//    }
//    
//    
//}
