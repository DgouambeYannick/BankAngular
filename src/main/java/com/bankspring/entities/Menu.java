///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.entities;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import java.io.Serializable;
//import java.util.Collection;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.ManyToMany;
//import javax.persistence.OneToMany;
//import javax.xml.bind.annotation.XmlTransient;
//
///**
// *
// * @author stage_IT6
// */
//@Entity
//public class Menu implements Serializable{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long codeMenu;
//    
//    private String Libelle;
//    
//    @OneToMany(mappedBy = "menu",fetch = FetchType.LAZY)
//    private Collection<Sousmenu> sousmenus;
//
//    public Menu() {
//    }
//
//    public Menu(String Libelle) {
//        this.Libelle = Libelle;
//    }
//
//    public Long getCodeMenu() {
//        return codeMenu;
//    }
//
//    public void setCodeMenu(Long codeMenu) {
//        this.codeMenu = codeMenu;
//    }
//
//    public String getLibelle() {
//        return Libelle;
//    }
//
//    public void setLibelle(String Libelle) {
//        this.Libelle = Libelle;
//    }
//    
//    @JsonIgnore
//    @XmlTransient
//    public Collection<Sousmenu> getSousmenus() {
//        return sousmenus;
//    }
//
//    public void setSousmenus(Collection<Sousmenu> sousmenus) {
//        this.sousmenus = sousmenus;
//    }
//    
//    
//}
