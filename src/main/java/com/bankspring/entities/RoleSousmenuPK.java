///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.entities;
//
//import java.io.Serializable;
//import javax.persistence.Column;
//import javax.persistence.Embeddable;
//import javax.validation.constraints.NotNull;
//
///**
// *
// * @author stage_IT6
// */
//@Embeddable
//public class RoleSousmenuPK implements Serializable{
//    @NotNull
//    @Column(name = "roles_id")
//    private Long roles_id;
//    @NotNull
//    @Column(name = "sousmenus_id")
//    private Long sousmenu_id;
//
//    public RoleSousmenuPK() {
//    }
//
//    public RoleSousmenuPK(Long roles_id, Long sousmenu_id) {
//        this.roles_id = roles_id;
//        this.sousmenu_id = sousmenu_id;
//    }
//
//    public Long getRoles_id() {
//        return roles_id;
//    }
//
//    public void setRoles_id(Long roles_id) {
//        this.roles_id = roles_id;
//    }
//
//    public Long getSousmenu_id() {
//        return sousmenu_id;
//    }
//
//    public void setSousmenu_id(Long sousmenu_id) {
//        this.sousmenu_id = sousmenu_id;
//    }
//    
//    
//}
