/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DGOUAMBE
 */
@Entity
public class Droits implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long droits_id;
    
    private String Libelle;
    
    @OneToMany(mappedBy = "droits",fetch = FetchType.LAZY)
    private Collection<RoleDroits> roledroits;

    public Droits() {
    }

    public Droits(String Libelle) {
        this.Libelle = Libelle;
    }

    public Long getDroits_id() {
        return droits_id;
    }

    public void setDroits_id(Long droits_id) {
        this.droits_id = droits_id;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String Libelle) {
        this.Libelle = Libelle;
    }
    @JsonIgnore
    @XmlTransient
    public Collection<RoleDroits> getRoledroits() {
        return roledroits;
    }

    public void setRoledroits(Collection<RoleDroits> roledroits) {
        this.roledroits = roledroits;
    }
    
    
}
