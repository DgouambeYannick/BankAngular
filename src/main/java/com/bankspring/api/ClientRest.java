/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.dao.ClientRepository;
import com.bankspring.entities.Client;
import com.bankspring.entities.Compte;
import com.bankspring.entities.User;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.security.response.DataResponse;
import com.bankspring.security.response.OperationResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DGOUAMBE
 */
@RestController
@RequestMapping("/api/client")
public class ClientRest {

    @Autowired
    private IBanqueMetier IB;
    @Autowired
    ClientRepository clientRepo;

    @ApiOperation(value = "Add new client", response = OperationResponse.class)
    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    public OperationResponse Addclient(@RequestBody Client c) {
        OperationResponse resp = new OperationResponse();
        try {

            if (clientRepo.exists(c.getCodeclient())) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
                resp.setOperationMessage("Le client existe deja ");
            } else {
                IB.Addclient(c);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Le Client " + c.getNomclient() + " a ete cree avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }

    @ApiOperation(value = "Get client", response = DataResponse.class)
    @RequestMapping(value = "/clientconsult/{id}", method = RequestMethod.GET)
    public DataResponse ConsulterClient(@PathVariable("id") Long idclient) {
        DataResponse resp = new DataResponse();
        try {
            Client c = IB.ConsulterClient(idclient);
            resp.setClient(c);
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }

    @ApiOperation(value = "Liste client", response = DataResponse.class)
    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public DataResponse Allclients() {
        DataResponse resp = new DataResponse();
        try {
            List<Client> lc = IB.Allclients();
            resp.setClients(lc);
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("All clients");
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }

    @RequestMapping(value = "/clientsearch/{nom}", method = RequestMethod.GET)
    public List<Client> Searchclients(@PathVariable String nom) {
        return IB.Searchclients(nom);
    }

    @ApiOperation(value = "Update Client", response = OperationResponse.class)
    @RequestMapping(value = "/clientUpdate/{id}", method = RequestMethod.GET)
    public OperationResponse UpdateClient(@PathVariable("id") Long id, @RequestBody Client c) {
        OperationResponse resp = new OperationResponse();
        try {
            if (IB.UpdateClient(c, id)) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Mise A jour effectuée avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
}
