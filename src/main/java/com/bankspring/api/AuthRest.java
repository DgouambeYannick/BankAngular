/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.dao.UserRepository;
import com.bankspring.entities.User;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.metier.UserService;
import com.bankspring.security.response.OperationResponse;
import com.bankspring.security.response.OperationResponse.ResponseStatusEnum;
import com.bankspring.security.response.UserResponse;
import com.bankspring.security.session.Login;
import com.bankspring.security.session.SessionItem;
import com.bankspring.security.session.SessionResponse;
import com.google.common.base.Strings;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author stage_IT6
 */
@RestController
@Api(tags = {"Authentication"})
public class AuthRest {

    @Autowired
    private IBanqueMetier IB;
    @Autowired
    private UserRepository userRepo;

    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Renvoie un jeton de sécurité, qui doit être transmis à chaque demande", response = SessionResponse.class)})
    @RequestMapping(value = "/session", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse newSession(@RequestBody Login login, HttpServletRequest request, HttpServletResponse response) {
        System.out.format("\n /Session Called username=%s\n", login.getUsername());
        User user = userRepo.findOneBynameAndPassword(login.getUsername(), login.getPassword()).orElse(null);
        SessionResponse resp = new SessionResponse();
        SessionItem sessionItem = new SessionItem();
        if (user != null) {
            System.out.format("\n /User Details=%s\n", user.getEmployeSup());
            sessionItem.setToken("xxx.xxx.xxx");
//            sessionItem.setUserId(user.getUserId());
//            sessionItem.setFirstName(user.getFirstName());
//            sessionItem.setLastName(user.getLastName());
//            sessionItem.setEmail(user.getEmail());
            //sessionItem.setRole(user.getRole());
            sessionItem.setEnabled(user.getenabled());
            sessionItem.setNomEmploye(user.getNomEmploye());
            sessionItem.setPhoto(user.getPhotouser());
            sessionItem.setUserId(user.getUsers_id());
            sessionItem.setUsername(user.getname());
            //sessionItem.setRoles(tokenUser.getUser().getRoles());

            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Succès de connexion factice");
            resp.setItem(sessionItem);
        } else {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage("Echec de connexion");
        }
        return resp;
    }  
    
    

}
