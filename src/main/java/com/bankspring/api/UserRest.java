/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.dao.UserRepository;
import com.bankspring.dto.UserDTO;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.metier.UserService;
import com.bankspring.security.response.OperationResponse;
import com.bankspring.security.response.OperationResponse.ResponseStatusEnum;
import com.bankspring.security.response.UserResponse;
import com.google.common.base.Strings;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DGOUAMBE
 */
@RestController
@RequestMapping("/api/user")
public class UserRest {

    @Autowired
    private UserService userService;
    @Autowired
    private IBanqueMetier IB;
    @Autowired
    UserRepository userRepo;

    @ApiOperation(value = "Add new user", response = OperationResponse.class)
    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public OperationResponse AddUser(List<Role> lr, User c, Long codesup) {
        OperationResponse resp = new OperationResponse();
        try {
            if (userRepo.exists(c.getUsers_id())) {
                resp.setOperationStatus(ResponseStatusEnum.ERROR);
                resp.setOperationMessage("L\'utilisateur existe deja ");
            } else {
                IB.AddUser(lr, c, codesup);
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("L\'Utilisateur " + c.getNomEmploye() + " a ete cree avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;

    }

////    @RequestMapping(value = "/userDTOget/{id}",method = RequestMethod.GET)
////    public UserDTO GetUserDTO(@PathVariable(name = "id") Long idemp) {
////        return IB.getDetialsUser(idemp);
////    }
//    
//    @RequestMapping(value = "/userget/{id}",method = RequestMethod.GET)
//    public User GetUser(@PathVariable(name = "id") Long idemp) {
//        return IB.GetUser(idemp);
//    }
//    
    @RequestMapping(value = "/userroles/{id}", method = RequestMethod.GET)
    public List<Role> GetRolesbyUser(@PathVariable(name = "id") Long idemp) {
        return IB.RoleByUser(idemp);
    }

    @ApiOperation(value = "Update User", response = OperationResponse.class)
    @RequestMapping(value = "/userupdate/{id}/{idsup}", method = RequestMethod.PUT)
    public OperationResponse UpdateUser(@PathVariable(name = "id") Long iduser,
            @PathVariable(name = "idsup") Long idsup,
            @RequestBody User u) {
        OperationResponse resp = new OperationResponse();
        try {
            IB.UpdateUser(u, iduser, idsup);
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("Mise A jour effectuée avec succes");
        } catch (Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }

    @ApiOperation(value = "Change statut User", response = OperationResponse.class)
    @RequestMapping(value = "/userchangeStatut/{id}", method = RequestMethod.PUT)
    public OperationResponse StatutUser(@PathVariable(name = "id") Long iduser) {
        OperationResponse resp = new OperationResponse();
        try {
            if (IB.DesactivedORActiveUser(iduser)) {
                resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Mise A jour Statut effectuée avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }

    @ApiOperation(value = "List User", response = UserResponse.class)
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public UserResponse AllUsers() {
        UserResponse resp = new UserResponse();
        try {
            List<User> list = IB.AllUser();
            resp.setUsers(list);
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
            resp.setOperationMessage("All users");
        } catch (Exception ex) {
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
//    
//    @RequestMapping(value = "/connexion",method = RequestMethod.POST)
//    public User Connexion(String user) {
//        return IB.Connexion(user);
//    }

    @ApiOperation(value = "Gets current user information", response = OperationResponse.class)
    @RequestMapping(value = "/user", method = RequestMethod.GET, produces = {"application/json"})
    public OperationResponse getUserInformation(@RequestParam(value = "name", required = false) String userIdParam, HttpServletRequest req) {

        String loggedInUserId = userService.getLoggedInUserId();

        User user;
        boolean provideUserDetails = false;

        if (Strings.isNullOrEmpty(userIdParam)) {
            provideUserDetails = true;
            user = userService.getLoggedInUser();
        } else if (loggedInUserId.equals(userIdParam)) {
            provideUserDetails = true;
            user = userService.getLoggedInUser();
        } else {
            //Check if the current user is superuser then provide the details of requested user
            provideUserDetails = true;
            user = userService.getUserInfoByUserId(userIdParam);
        }

        OperationResponse resp = new OperationResponse();
        if (provideUserDetails) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
        } else {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.NO_ACCESS);
            resp.setOperationMessage("Pas Acces");
        }
        //resp.setData(user);
        return resp;
    }
    
    @ApiOperation(value = "desconnexion", response = OperationResponse.class)
    @RequestMapping(value = "/logout", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
    public OperationResponse logout(HttpServletRequest req) {
        OperationResponse resp = new OperationResponse();
        if(userService.Logout()){
            resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
        }else{
            resp.setOperationStatus(ResponseStatusEnum.ERROR);
        }
        return resp;
    }
}
