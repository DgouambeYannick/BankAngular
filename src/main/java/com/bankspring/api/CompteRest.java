/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.dao.CompteRepository;
import com.bankspring.entities.Compte;
import com.bankspring.entities.CompteCourant;
import com.bankspring.entities.CompteEpargne;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.security.response.DataResponse;
import com.bankspring.security.response.OperationResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DGOUAMBE
 */
@RestController
@RequestMapping("/api/compte")
public class CompteRest {
    @Autowired
    private IBanqueMetier IB;
    @Autowired
    CompteRepository compteRepo;
    
    @ApiOperation(value = "Add new compte", response = OperationResponse.class)
    @RequestMapping(value = "/comptes/{idclt}/{iduser}", method = RequestMethod.POST)
    public OperationResponse AddCompte(@RequestBody Compte c, @PathVariable("idclt") Long idclt, @PathVariable("iduser") Long iduser) { 
         OperationResponse resp = new OperationResponse();
        try {

            if (compteRepo.exists(c.getIdompte())) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
                resp.setOperationMessage("Le Compte existe deja ");
            } else {
                IB.AddCompte(c, idclt, iduser);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Le Compte " + c.getCodeCompte() + " a ete cree avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    @ApiOperation(value = "Get compte", response = DataResponse.class)
    @RequestMapping(value = "/comptesearch/{codecompte}", method = RequestMethod.GET)
    public DataResponse searchCompte(@PathVariable(name = "codecompte") String codecompte) {
        DataResponse resp = new DataResponse();
        try {
            Compte c = IB.searchCompte(codecompte);
            if (c instanceof CompteCourant) {
                resp.setComptecourant((CompteCourant)c);
            }
            if (c instanceof CompteEpargne) {
                resp.setCompteEpargne((CompteEpargne)c);
            }
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    @RequestMapping(value = "/compteget/{id}", method = RequestMethod.GET)
    public Compte getCompte(@PathVariable(name = "id") Long idcompt) {
        return IB.getCompte(idcompt);
    }
    
    @RequestMapping(value = "/comptes", method = RequestMethod.GET)
    public List<Compte> Allcomptes() {
        return IB.Allcomptes();
    }
    
    @RequestMapping(value = "/comptesbyclient/{id}",method = RequestMethod.GET)
    public List<Compte> ComptesByClient(@PathVariable Long idclient) {
        return IB.ComptesByClient(idclient);
    }
}
