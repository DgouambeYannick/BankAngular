/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.entities.Operation;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.security.response.ListeResponse;
import com.bankspring.security.response.OperationResponse;
import io.swagger.annotations.ApiOperation;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author DGOUAMBE
 */
@RestController
@RequestMapping("api/operation")
public class OperationRest {
    @Autowired
    private IBanqueMetier IB;
    
     @ApiOperation(value = "Add versement", response = OperationResponse.class)
    @RequestMapping(value = "/versement", method = RequestMethod.PUT)
    public OperationResponse versement(@RequestParam Long idcompt, 
            @RequestParam double prix, 
            @RequestParam Long idemp) {
        OperationResponse resp = new OperationResponse();
        try {
            if (IB.versement(idcompt, prix, idemp)) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Versement effectue avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    @ApiOperation(value = "Add retrait", response = OperationResponse.class)
    @RequestMapping(value = "/retrait", method = RequestMethod.PUT)
    public OperationResponse retrait(@RequestParam Long idcompt, 
            @RequestParam double prix, 
            @RequestParam Long idemp) {
        OperationResponse resp = new OperationResponse();
        try {
            if (IB.retrait(idcompt, prix, idemp)) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Retrait effectue avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    @ApiOperation(value = "Add virement", response = OperationResponse.class)
    @RequestMapping(value = "/virement", method = RequestMethod.PUT)
    public OperationResponse virement(@RequestParam Long idcompt1, 
            @RequestParam Long idcompt2, 
            @RequestParam double prix, 
            @RequestParam Long idemp) {
        OperationResponse resp = new OperationResponse();
        try {
            if (IB.virement(idcompt1, idcompt2, prix, idemp)) {
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
                resp.setOperationMessage("Virement effectue avec succes");
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    @ApiOperation(value = "Get Operations", response = ListeResponse.class)
    @RequestMapping(value = "/operations", method = RequestMethod.GET)
    public ListeResponse listOperation(
            @RequestParam(name = "idcompte") Long codecompte, 
            @RequestParam(name = "page", defaultValue = "0") int page, 
            @RequestParam(name = "size", defaultValue = "5") int size) {
        
        ListeResponse resp = new ListeResponse();
        try {
            if (codecompte != null) {
                Page<Operation> lo = IB.listOperation(codecompte, page, size);
                resp.setOperations(lo.getContent());
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            }
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;
    }
    
    
    
}
