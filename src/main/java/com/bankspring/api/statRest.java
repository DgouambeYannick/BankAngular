/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.api;

import com.bankspring.dao.UserRepository;
import com.bankspring.metier.IBanqueMetier;
import com.bankspring.security.response.DashboardResponse;
import com.bankspring.security.response.OperationResponse;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author stage_IT6
 */
@RestController
@RequestMapping("/api/stats")
public class statRest {
    @Autowired
    private IBanqueMetier IB;
    
    @ApiOperation(value = "nombre compte courant", response = DashboardResponse.class)
    @RequestMapping(value = "/nbreCC/{id}", method = RequestMethod.GET)
    public DashboardResponse nbreCC(@PathVariable("id") Long codesup, HttpServletResponse res) {
        DashboardResponse resp = new DashboardResponse();
        try {
                Integer nb = IB.NbreCompteCourant(codesup);
                resp.setNbreCC(nb);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;

    }
    
    @ApiOperation(value = "nombre compte epargne", response = DashboardResponse.class)
    @RequestMapping(value = "/nbreCE/{id}", method = RequestMethod.GET)
    public DashboardResponse nbreCE(@PathVariable("id") Long codesup, HttpServletResponse res) {
        DashboardResponse resp = new DashboardResponse();
        try {
                Integer nb = IB.NbreCompteEpargne(codesup);
                resp.setNbreCE(nb);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;

    }
    
    @ApiOperation(value = "nombre Versement", response = DashboardResponse.class)
    @RequestMapping(value = "/nbreVers/{id}", method = RequestMethod.GET)
    public DashboardResponse nbreVers(@PathVariable("id") Long codesup, HttpServletResponse res) {
        DashboardResponse resp = new DashboardResponse();
        try {
                Integer nb = IB.NbreVersement(codesup);
                resp.setNbreVersement(nb);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;

    }
    
    @ApiOperation(value = "nombre Retrait", response = DashboardResponse.class)
    @RequestMapping(value = "/nbreRetrait/{id}", method = RequestMethod.GET)
    public DashboardResponse nbreRetrait(@PathVariable("id") Long codesup, HttpServletResponse res) {
        DashboardResponse resp = new DashboardResponse();
        try {
                Integer nb = IB.NbreRetrait(codesup);
                resp.setNbreRetrait(nb);
                resp.setOperationStatus(OperationResponse.ResponseStatusEnum.SUCCESS);
            
        } catch (Exception ex) {
            resp.setOperationStatus(OperationResponse.ResponseStatusEnum.ERROR);
            resp.setOperationMessage("INFOS :" + ex.getMessage());
        }
        return resp;

    }
}
