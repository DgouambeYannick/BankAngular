package com.bankspring.dao;

import com.bankspring.entities.Compte;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompteRepository extends JpaRepository<Compte, Long>{
    @Query("select c from Compte c where c.codeCompte =:code")
    public Compte getcompte(@Param("code")String codeCompte);
    
    @Query("Select o from Compte o where o.client.codeclient =:id")
    public List<Compte> consulterclientbycompte(@Param("id")Long clt);
    
    @Query("select c from Compte c where c.user.users_id =:n")
    public List<Compte> AllCompteByUser(@Param("n")Long iduser);
    
}
