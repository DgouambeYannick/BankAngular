package com.bankspring.dao;

import com.bankspring.entities.Operation;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OperationRepository extends JpaRepository<Operation, Long>{
    @Query("select o from Operation o where o.compte.idompte =:id ORDER BY o.numeroOperation DESC")
    public Page<Operation> listOperation (@Param("id")Long codecpt, Pageable pageable);
    
    @Query("select o from Operation o where o.user.users_id =:id")
    public List<Operation> listVersement (@Param("id")Long iduser);
    
    @Query("select o from Operation o where o.user.users_id =:id")
    public List<Operation> listRetrait (@Param("id")Long iduser);
    
    
}
