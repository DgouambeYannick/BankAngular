/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dao;

import com.bankspring.entities.RoleDroits;
import com.bankspring.entities.RoleDroitsPK;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author DGOUAMBE
 */
public interface RoleDroitsRepository extends JpaRepository<RoleDroits, RoleDroitsPK>{
     @Query("Select o from RoleDroits o where o.roleDroitsPK.roles_id =:id")
    public List<RoleDroits> ListRoledroitsbyRole(@Param("id")Long id);
}
