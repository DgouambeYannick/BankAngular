///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.dao;
//
//import com.bankspring.entities.RoleSousmenu;
//import com.bankspring.entities.RoleSousmenuPK;
//import com.bankspring.entities.UserRole;
//import java.util.List;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
///**
// *
// * @author stage_IT6
// */
//public interface RoleSousmenuRepository extends JpaRepository<RoleSousmenu, RoleSousmenuPK>{
//    @Query("Select o from RoleSousmenu o where o.roleSousmenuPK.roles_id =:id")
//    public List<RoleSousmenu> ListRoleSousmenubyRole(@Param("id")Long id);
//}
