/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dao;

import com.bankspring.entities.Droits;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author DGOUAMBE
 */
public interface DroitsRepository extends JpaRepository<Droits, Long>{
    
}
