///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.dao;
//
//import com.bankspring.entities.Sousmenu;
//import java.io.Serializable;
//import java.util.List;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
///**
// *
// * @author stage_IT6
// */
//public interface SousmenuRepository extends JpaRepository<Sousmenu, Long>{
//    
//    @Query("Select o from Sousmenu o where o.menu.codeMenu =:id")
//    public List<Sousmenu> ListSousmBymenu(@Param("id")Long clt);
//}
