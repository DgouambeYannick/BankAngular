package com.bankspring.dao;

import com.bankspring.entities.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long>{
    @Query("select c from User c where c.name =:n")
    public User connect(@Param("n")String username);
    
    Optional<User> findOneByname(String name);
    Optional<User> findOneBynameAndPassword(String name, String password);
}
