/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dao;

import com.bankspring.entities.UserRole;
import com.bankspring.entities.UserRolePK;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author stage_IT6
 */
public interface UserRoleRepository extends JpaRepository<UserRole, UserRolePK>{
    
    @Query("Select o from UserRole o where o.userRolePK.users_id =:id")
    public List<UserRole> ListUserRolebyUser(@Param("id")Long id);
}
