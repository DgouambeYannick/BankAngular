package com.bankspring.dao;

import com.bankspring.entities.Client;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends JpaRepository<Client, Long> {
    
    @Query("Select o from Client o where o.nomclient LIKE :x")
    public List<Client> searchclient(@Param("x")String nom);
}
