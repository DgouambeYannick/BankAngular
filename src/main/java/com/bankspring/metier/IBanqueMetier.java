/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.metier;

import com.bankspring.dto.UserDTO;
import com.bankspring.entities.Client;
import com.bankspring.entities.Compte;
import com.bankspring.entities.Droits;
import com.bankspring.entities.Operation;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import java.util.List;
import org.springframework.data.domain.Page;

/**
 *
 * @author DGOUAMBE
 */
public interface IBanqueMetier {
    public Client Addclient(Client c);
    public User AddUser(List<Role> lr,User c, Long codesup);
    
//    public UserDTO Connexion(String user);
    public User GetUser(Long iduser);
    public Role AddRole(List<Droits> ls, Role c);
    public Compte AddCompte(Compte c, Long idclt, Long idemp);
    public Operation AddOperation(Operation o, Long idcompt, Long idemp);
    public boolean versement(Long idcompt, double prix, Long idemp);
    public boolean retrait(Long idcompt, double prix, Long idemp);
    public boolean virement(Long idcompt1, Long idcompt2, double prix, Long idemp);
    
    public Compte searchCompte(String codecompte);
    public Compte getCompte(Long idcompt);
    public Role getRole(Long idrole);
    
    public Client ConsulterClient(Long idclient);
    public List<Client> Allclients();
    public List<Client> Searchclients(String nom);
    public List<Compte> ComptesByClient(Long idclient);
    
    public List<Role> RoleByUser(Long idemp);
    public List<Droits> DroitsByRole(Long role);
//    public List<Sousmenu> SousmenuByRole(Long role);
//    public List<Sousmenu> SousmenuByMenu(Long menu);
    public List<User> AllUser();
    public List<Role> ConsulterRoLe();
    public Page<Operation> listOperation(Long codecompte, int page, int size);
    
    public List<Compte> Allcomptes();
//    public Menu AddMenu(Menu m);
//    public Sousmenu AddSousMenu(Sousmenu m);
    public Droits AddDroits(Droits d);
    
//    public UserDTO getDetialsUser(Long iduser);
    public boolean UpdateCompte(Compte c, Long id, Long clt, Long user);
    public boolean UpdateClient(Client c, Long id);
    public boolean UpdateUser(User c, Long id, Long idsup);
    public boolean DesactivedORActiveUser(Long id);
    
    public Integer NbreCompteCourant(Long id);
    public Integer NbreCompteEpargne(Long id);
    
    public Integer NbreVersement(Long id);
    public Integer NbreRetrait(Long id);
}
