/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.metier;

import com.bankspring.dto.UserDTO;
import com.bankspring.dao.ClientRepository;
import com.bankspring.dao.CompteRepository;
import com.bankspring.dao.DroitsRepository;
import com.bankspring.dao.OperationRepository;
import com.bankspring.dao.RoleDroitsRepository;
import com.bankspring.dao.RoleRepository;
import com.bankspring.dao.UserRepository;
import com.bankspring.dao.UserRoleRepository;
import com.bankspring.entities.Client;
import com.bankspring.entities.Compte;
import com.bankspring.entities.CompteCourant;
import com.bankspring.entities.CompteEpargne;
import com.bankspring.entities.Droits;
import com.bankspring.entities.Operation;
import com.bankspring.entities.Retrait;
import com.bankspring.entities.Role;
import com.bankspring.entities.RoleDroits;
import com.bankspring.entities.User;
import com.bankspring.entities.UserRole;
import com.bankspring.entities.Versement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author DGOUAMBE
 */
@Service
@Transactional
public class BankmetierImpl implements IBanqueMetier {

    @Autowired
    private CompteRepository compteRepo;
    @Autowired
    private ClientRepository clientRepo;
    @Autowired
    private OperationRepository operationRepo;
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private RoleRepository roleRepo;
//    @Autowired
//    private MenuRepository menuRepo;
//    @Autowired
//    private SousmenuRepository sousmenuRepo;
    @Autowired
    private UserRoleRepository userRoleRepo;
//    @Autowired
//    private RoleSousmenuRepository roleSousmenuRepo;
    @Autowired
    private RoleDroitsRepository roleDroitsRepo;
    @Autowired
    private DroitsRepository DroitsRepo;

    @Override
    public User AddUser(List<Role> lr, User c, Long codesup) {
        //   try {
        if (codesup != null) {
            User sup = GetUser(codesup);
            c.setEmployeSup(sup);
        }
        //  Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        BCryptPasswordEncoder encod = new BCryptPasswordEncoder();
        //c.setPassword(encoder.encodePassword(c.getPassword(), c));
        c.setPassword(encod.encode(c.getPassword()));
        //c.setPassword(c.getPassword());
        c.setenabled(1);
        userRepo.save(c);

        if (lr != null) {
            for (Role r : lr) {
                UserRole ur = new UserRole(c.getUsers_id(), r.getRoles_id());
                userRoleRepo.save(ur);
//                AddEmployeToGroupe(c.getCodeEmploye(), r.getCodeGroupe());
            }
        } else {
            throw new RuntimeException("Attention!! cet utilisateur n\'a pas de role rattache");
        }
//        
        return c;
//        } catch (Exception e) {
//            throw new RuntimeException(e.getMessage() +" "+ c.getNomEmploye());
//        }
    }

    @Override
    public List<User> AllUser() {
        List<User> lu = userRepo.findAll();
        if (lu.isEmpty()) {
            throw new RuntimeException("Aucun utilisateur trouvé");
        } else {
            return lu;
        }
    }

    @Override
    public User GetUser(Long idemp) {
        User em = userRepo.findOne(idemp);
        if (em == null) {
            throw new RuntimeException("Utilisateur Introuvable");
        }
        return em;
    }

    /**
     * *********ROLE***
     */
    @Override
    public Role AddRole(List<Droits> ls, Role r) {
        roleRepo.save(r);
        if (ls != null) {
//                for (Sousmenu Sm : ls) {
//                    RoleSousmenu rs = new RoleSousmenu(r.getRoles_id(), Sm.getSousmenus_id());
//                    roleSousmenuRepo.save(rs);
//                }
            for (Droits Sm : ls) {
                RoleDroits rs = new RoleDroits(r.getRoles_id(), Sm.getDroits_id());
                roleDroitsRepo.save(rs);
            }
            return r;
        } else {
            throw new RuntimeException("Attention!! ce role n\'a pas de Droits rattache");
        }
    }

    @Override
    public Role getRole(Long idrole) {
        Role ro = roleRepo.findOne(idrole);
        if (ro == null) {
            throw new RuntimeException("Role Introuvale");
        }
        return ro;
    }

    @Override
    public List<Role> ConsulterRoLe() {
        List<Role> lr = roleRepo.findAll();
        if (lr.isEmpty()) {
            throw new RuntimeException("Aucun role trouvé");
        } else {
            return lr;
        }
    }

    @Override
    public List<Role> RoleByUser(Long idemp) {
        List<Role> Lr = new ArrayList<>();
        List<UserRole> ur = userRoleRepo.ListUserRolebyUser(idemp);
        if (ur != null) {
            for (UserRole userR : ur) {
                Role r = getRole(userR.getUserRolePK().getRoles_id());
                Lr.add(r);
            }
            return Lr;
        } else {
            throw new RuntimeException("Liste des roles de l\' utilisateur " + GetUser(idemp).getNomEmploye() + " Introuvable");
        }
    }

    @Override
    public List<Droits> DroitsByRole(Long role) {
        List<Droits> Ls = new ArrayList<>();
        List<RoleDroits> rs = roleDroitsRepo.ListRoledroitsbyRole(role);
        if (rs != null) {
            for (RoleDroits r : rs) {
                Droits s = GetDroits(r.getRoleDroitsPK().getDroits_id());
                Ls.add(s);
            }
            return Ls;
        } else {
            throw new RuntimeException("Liste des droits du role " + getRole(role).getname() + " Introuvable");
        }
    }

    @Override
    public Droits AddDroits(Droits d) {
        return DroitsRepo.save(d);
    }

    public Droits GetDroits(Long d) {
        Droits ro = DroitsRepo.findOne(d);
        if (ro == null) {
            throw new RuntimeException("Droit Introuvale");
        }
        return ro;
    }

    /**
     * ********COMPTE**********
     */
    @Override
    public Compte AddCompte(Compte c, Long idclt, Long idemp) {
        try {
            User a = userRepo.findOne(idemp);
            Client cl = ConsulterClient(idclt);
            c.setClient(cl);
            c.setUser(a);
            c.setDatecreate(new Date());
            compteRepo.save(c);
            return c;
        } catch (Exception e) {
            throw new RuntimeException("Oups!! Erreur survenue lors du retour des informations du client");
        }
    }

    @Override
    public Page<Operation> listOperation(Long codecompte, int page, int size) {
        return operationRepo.listOperation(codecompte, new PageRequest(page, size));
    }

    @Override
    public List<Compte> Allcomptes() {
        List<Compte> lc = compteRepo.findAll();
        return lc;
    }

    public Operation AddOperation(Operation o, Long idcompt, Long idemp) {
        Compte c = getCompte(idcompt);
        User u = GetUser(idemp);
        o.setCompte(c);
        o.setDateOperation(new Date());
        o.setUser(u);
        operationRepo.save(o);
        return o;
    }

    @Override
    public boolean versement(Long idcompt, double prix, Long idemp) {
        try {
            Compte c = getCompte(idcompt);
            AddOperation(new Versement(new Date(), prix), idcompt, idemp);
            c.setSolde(c.getSolde() + prix);
            compteRepo.save(c);
            //To change body of generated methods, choose Tools | Templates.
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Oups!! Erreur survenue lors de l'enregistrement du versement");
        }
    }

    @Override
    public boolean retrait(Long idcompt, double prix, Long idemp) {
        Compte c = getCompte(idcompt);
        double facilitecaisse = 0;
        if (c instanceof CompteCourant) {
            facilitecaisse = (((CompteCourant) c).getDecouvert());
        }
        if (c.getSolde() + facilitecaisse < prix) {
            throw new RuntimeException("Solde Insuffisant");
        }

        AddOperation(new Retrait(new Date(), prix), idcompt, idemp);
        c.setSolde(c.getSolde() - prix);
        compteRepo.save(c);

        return true;
    }

    @Override
    public boolean virement(Long idcompt1, Long idcompt2, double prix, Long idemp) {
        if (idcompt1.equals(idcompt2)) {
            throw new RuntimeException("Virement Impossible sur le meme Compte");
        } else {
            retrait(idcompt1, prix, idemp);
            versement(idcompt2, prix, idemp);
        }
        return true;
    }

    @Override
    public Compte searchCompte(String codecompte) {
        Compte c = compteRepo.getcompte(codecompte);
        if (c == null) {
            throw new RuntimeException("compte introuvable");
        }
        return c;
    }

    @Override
    public Compte getCompte(Long idcompt) {
        Compte cp = compteRepo.findOne(idcompt);
        if (cp == null) {
            throw new RuntimeException("Compte Introuvale");
        }
        return cp;
    }

    /**
     * ******CLIENT********
     */
    @Override
    public Client Addclient(Client c) {
        return clientRepo.save(c);
    }

    @Override
    public Client ConsulterClient(Long idclient) {
        Client c = clientRepo.findOne(idclient);
        if (c == null) {
            throw new RuntimeException("Client Introuvable");
        }
        return c;
    }

    @Override
    public List<Client> Allclients() {
        List<Client> l = clientRepo.findAll();
        if (l != null) {
            return l;
        } else {
            throw new RuntimeException("Aucun role trouvé");
        }
    }

    @Override
    public List<Client> Searchclients(String nom) {
        List<Client> s = clientRepo.searchclient(nom);
        return s;
    }

    @Override
    public List<Compte> ComptesByClient(Long idclient) {
        List<Compte> c = compteRepo.consulterclientbycompte(idclient);
        if (c != null) {
            return c;
        } else {
            throw new RuntimeException("Aucun Compte du client " + ConsulterClient(idclient).getNomclient() + " trouvé");
        }
    }

//    @Override
//    public List<UserDTO> AllUser() {
//        return userRepo.findAll();
//    }
//    @Override
//    public Menu AddMenu(Menu c) {
//        menuRepo.save(c);
//        if (c.getSousmenus() != null) {
//            for (Sousmenu sm : c.getSousmenus()) {
//                sm.setMenu(c);
//                sousmenuRepo.save(sm);
//            }
//        } else {
//            throw new RuntimeException("Attention!! Ce Menu ne contient pas de Sous Menu");
//        }
//        return c;
//    }
//
//    @Override
//    public Sousmenu AddSousMenu(Sousmenu s) {
//        return sousmenuRepo.save(s);
//    }
//
//    public Sousmenu getSousmenu(Long id) {
//        Sousmenu ro = sousmenuRepo.findOne(id);
//        if (ro == null) {
//            throw new RuntimeException("Sousmenu Introuvale");
//        }
//        return ro;
//    }
//
//    @Override
//    public List<Sousmenu> SousmenuByRole(Long role) {
//        List<Sousmenu> Ls = new ArrayList<>();
//        List<RoleSousmenu> rs = roleSousmenuRepo.ListRoleSousmenubyRole(role);
//        if (rs != null) {
//            for (RoleSousmenu r : rs) {
//                Sousmenu s = getSousmenu(r.getRoleSousmenuPK().getSousmenu_id());
//                Ls.add(s);
//            }
//            return Ls;
//        } else {
//            throw new RuntimeException("Liste des sous menus du role " + getRole(role).getname() + " Introuvable");
//        }
//    }
//    @Override
//    public UserDTO getDetialsUser(Long iduser) {
//        List<Role> lr = new ArrayList<>();
//        List<Sousmenu> lsm = new ArrayList<>();
//        List<Sousmenu> lsm2 = new ArrayList<>();
//        UserDTO udto = new UserDTO(GetUser(iduser));
//        lr = RoleByUser(iduser);
//        if (lr != null) {
//            for (Role ro : lr) {
//                lsm2 = SousmenuByRole(ro.getRoles_id());
//                if (lsm2 == null) {
//                    throw new RuntimeException("Liste des sous menus du role " + getRole(ro.getRoles_id()).getname() + " Introuvable");
//                }
//                for (Sousmenu sousmenu : lsm2) {
//                    lsm.add(sousmenu);
//                }
//            }
//            udto.setRolesUSER(lr);
//            udto.setSousmenuUSER(lsm);
//            return udto;
//        } else {
//            throw new RuntimeException("Liste des roles de l\'utilisateur " + GetUser(iduser).getNomEmploye() + " Introuvable");
//        }
//    }
//    @Override
//    public List<Sousmenu> SousmenuByMenu(Long menu) {
//        List<Sousmenu> Ls = sousmenuRepo.ListSousmBymenu(menu);
//        if (Ls != null) {
//            return Ls;
//        } else {
//            throw new RuntimeException("Liste des sous menus du Menu .... Introuvable");
//        }
//    }
    @Override
    public boolean UpdateCompte(Compte c, Long id, Long clt, Long user) {
        c.setIdompte(id);
        c.setClient(ConsulterClient(clt));
        c.setUser(GetUser(user));
        compteRepo.save(c);
        return true;
    }

    @Override
    public boolean UpdateClient(Client c, Long id) {
        c.setCodeclient(id);
        clientRepo.save(c);
        return true;
    }

    @Override
    public boolean UpdateUser(User c, Long id, Long idsup) {
        c.setUsers_id(id);
        if (idsup != null) {
            c.setEmployeSup(GetUser(idsup));
        }
        c.setUsers_id(id);
        userRepo.save(c);
        return true;
    }

    @Override
    public boolean DesactivedORActiveUser(Long id) {
        User us = GetUser(id);
        Integer f = us.getenabled();
        // out.print("st="+f);
        if (f.equals(1)) {
            f = 0;
        } else {
            f = 1;
        }
        us.setenabled(f);
        userRepo.save(us);
        return true;
    }

    @Override
    public Integer NbreCompteCourant(Long id) {
        List<Compte> all2 = new ArrayList<>();
        List<Compte> all = compteRepo.AllCompteByUser(id);
        if (all != null) {
            for (Compte compte : all) {
                if (compte instanceof CompteCourant) {
                    all2.add(compte);
                }
            }
            return all2.size();
        }else{
            return 0;
        }
    }

    @Override
    public Integer NbreCompteEpargne(Long id) {
        List<Compte> al2 = new ArrayList<>();
        List<Compte> al = compteRepo.AllCompteByUser(id);
        if (al != null) {
            for (Compte compte : al) {
                if (compte instanceof CompteEpargne) {
                    al2.add(compte);
                }
            }
            return al2.size();
        }else{
            return 0;
        }
    }

    @Override
    public Integer NbreVersement(Long id) {
        List<Operation> al2 = new ArrayList<>();
        List<Operation> al = operationRepo.listVersement(id);
        if (al != null) {
            for (Operation Oper : al) {
                if (Oper instanceof Versement) {
                    al2.add(Oper);
                }
            }
            return al2.size();
        }else{
            return 0;
        }
    }

    @Override
    public Integer NbreRetrait(Long id) {
        List<Operation> al2 = new ArrayList<>();
        List<Operation> al = operationRepo.listRetrait(id);
        if (al != null) {
            for (Operation Oper : al) {
                if (Oper instanceof Retrait) {
                    al2.add(Oper);
                }
            }
            return al2.size();
        }else{
            return 0;
        }
    }

}
