package com.bankspring;

import com.bankspring.entities.Client;
import com.bankspring.entities.Compte;
import com.bankspring.entities.CompteCourant;
import com.bankspring.entities.CompteEpargne;
import com.bankspring.entities.Droits;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import com.bankspring.metier.IBanqueMetier;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankspringApplication {

    @Autowired
    private IBanqueMetier IB;

    public static void main(String[] args) {
        SpringApplication.run(BankspringApplication.class, args);
    }

//    @Override
//    public void run(String... strings) throws Exception {
//        List<Droits> ListdroitAD = new ArrayList<>();
//        List<Droits> ListdroitCA = new ArrayList<>();
//        List<Droits> ListdroitUS = new ArrayList<>();
//        
//        List<Role> ListrolAD = new ArrayList<>();
//        List<Role> ListroCA = new ArrayList<>();
//        List<Role> ListrolUS = new ArrayList<>();
//        try {
//            Droits droit = new Droits("Operations.consulterCompte-addretrait");
//            ListdroitAD.add(droit);
//            ListdroitCA.add(droit);
//            IB.AddDroits(droit);
//            Droits droit1 = new Droits("Operations.consulterCompte-addvirement");
//            ListdroitAD.add(droit1);
//            ListdroitCA.add(droit1);
//            IB.AddDroits(droit1);
//            Droits droit2 = new Droits("Operations.consulterCompte-addversement");
//            ListdroitAD.add(droit2);
//            ListdroitCA.add(droit2);
//            IB.AddDroits(droit2);
//            Droits droit3 = new Droits("Operations.consulterCompte-view");
//            ListdroitAD.add(droit3);
//            ListdroitCA.add(droit3);
//            ListdroitUS.add(droit3);
//            IB.AddDroits(droit3);
//            Droits droit4 = new Droits("Operations.creerCompte-add");
//            ListdroitAD.add(droit4);
//            ListdroitCA.add(droit4);
//            IB.AddDroits(droit4);
//            Droits droit5 = new Droits("Operations.creerCompte-view");
//            ListdroitAD.add(droit5);
//            ListdroitCA.add(droit5);
//            IB.AddDroits(droit5);
//            Droits droit6 = new Droits("Operations.listeCompte-edit");
//            ListdroitAD.add(droit6);
//            IB.AddDroits(droit6);
//            Droits droit7 = new Droits("Operations.listeCompte-view");
//            ListdroitAD.add(droit7);
//            ListdroitCA.add(droit7);
//            IB.AddDroits(droit7);
//            Droits droit8 = new Droits("Operations.listeCompte-delete");
//            ListdroitAD.add(droit8);
//            IB.AddDroits(droit8);
//            Droits droit9 = new Droits("Operations.listeCompte-update");
//            ListdroitAD.add(droit9);
//            IB.AddDroits(droit9);
//            Droits droit10 = new Droits("Assistance.CreerClient-add");
//            ListdroitAD.add(droit);
//            ListdroitCA.add(droit6);
//            IB.AddDroits(droit10);
//            Droits droit11 = new Droits("Assistance.CreerClient-view");
//            ListdroitAD.add(droit11);
//            ListdroitCA.add(droit11);
//            IB.AddDroits(droit11);
//            Droits droit12 = new Droits("Assistance.ListeClient-edit");
//            ListdroitAD.add(droit12);
//            ListdroitCA.add(droit12);
//            ListdroitUS.add(droit12);
//            IB.AddDroits(droit12);
//            Droits droit13 = new Droits("Assistance.ListeClient-view");
//            ListdroitAD.add(droit13);
//            ListdroitCA.add(droit13);
//            IB.AddDroits(droit13);
//            Droits droit14 = new Droits("Assistance.ListeClient-delete");
//            ListdroitAD.add(droit14);
//            IB.AddDroits(droit14);
//            Droits droit15 = new Droits("Assistance.ListeClient-update");
//            ListdroitAD.add(droit15);
//            IB.AddDroits(droit15);
//            Droits droit16 = new Droits("Administration.CreerUtilisateur-add");
//            ListdroitAD.add(droit16);
//            IB.AddDroits(droit16);
//            Droits droit17 = new Droits("Administration.CreerUtilisateur-view");
//            ListdroitAD.add(droit17);
//            IB.AddDroits(droit17);
//            Droits droit18 = new Droits("Administration.ListeUtilisateur-edit");
//            ListdroitAD.add(droit18);
//            IB.AddDroits(droit18);
//            Droits droit19 = new Droits("Administration.ListeUtilisateur-view");
//            ListdroitAD.add(droit19);
//            IB.AddDroits(droit19);
//            Droits droit20 = new Droits("Administration.ListeUtilisateur-delete");
//            ListdroitAD.add(droit20);
//            IB.AddDroits(droit20);
//            Droits droit21 = new Droits("Administration.ListeUtilisateur-update");
//            ListdroitAD.add(droit21);
//            IB.AddDroits(droit21);
//            Droits droit22 = new Droits("Administration.profils-view");
//            ListdroitAD.add(droit22);
//            IB.AddDroits(droit22);
//            Droits droit23 = new Droits("Administration.profils-save");
//            ListdroitAD.add(droit23);
//            IB.AddDroits(droit23);
//            
//            Role r1 = new Role("ADMIN");
//            ListrolAD.add(r1);
//            IB.AddRole(ListdroitAD, r1);
//            /**
//             * ***************
//             */
//            Role r2 = new Role("CAISSE");
//            ListrolAD.add(r2);
//            ListroCA.add(r2);
//           // r2.setSousmenus(ListsmCA);
//            IB.AddRole(ListdroitCA, r2);
//            /**
//             * ***************
//             */
//            Role r3 = new Role("USER");
//            ListrolAD.add(r3);
//            ListrolUS.add(r3);
//           // r3.setSousmenus(ListsmUS);
//            IB.AddRole(ListdroitUS, r3);
//            /**
//             * ****************
//             */
//            Client c1 = IB.Addclient(new Client("dgouambe", "bssadi"));
//            Client c2 = IB.Addclient(new Client("saah", "kotto"));
//            Client c3 = IB.Addclient(new Client("Tankeu", "logbessou"));
//            
//            User u1 = new User("FANSI", "admin", "admin");
//            IB.AddUser(ListrolAD, u1, null);
//            
//            User u2 = new User("TANKEU", "caisse", "caisse");
//            IB.AddUser(ListroCA, u2, u1.getUsers_id());
//            
//            User u3 = new User("TOTO", "user", "user");
//            IB.AddUser(ListrolUS, u3, u1.getUsers_id());
//            
//            IB.AddCompte(new CompteCourant(2000, "c1", new Date(), 50000), c1.getCodeclient(), u1.getUsers_id());
//            IB.AddCompte(new CompteEpargne(5.5, "c2", new Date(), 100000), c1.getCodeclient(), u1.getUsers_id());
//
//            IB.versement(c1.getCodeclient(), 10000, u1.getUsers_id());
//            IB.versement(c1.getCodeclient(), 5000, u1.getUsers_id());
//            IB.versement(c1.getCodeclient(), 5000, u1.getUsers_id());
//            IB.retrait(c1.getCodeclient(), 10000, u2.getUsers_id());
//
//            IB.versement(c2.getCodeclient(), 5000, u1.getUsers_id());
//            IB.versement(c2.getCodeclient(), 5000, u1.getUsers_id());
//            IB.versement(c2.getCodeclient(), 5000, u1.getUsers_id());
//            IB.retrait(c2.getCodeclient(), 10000, u2.getUsers_id());
//            
//        } catch (Exception ex) {
//            System.out.println("Infos : "+ex.getMessage());
//        }
//
//    }

}
