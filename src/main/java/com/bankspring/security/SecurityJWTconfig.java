///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bankspring.security;
//
//import java.util.Collections;
//import javax.servlet.Filter;
////import com.bankspring.security.JwtAuthenticationEntryPoint;
////import com.bankspring.security.JwtSuccessHandler;
////import com.bankspring.security.JwtauthenticationProvider;
////import com.bankspring.security.JwtauthenticationTokenFilter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.ProviderManager;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.http.SessionCreationPolicy;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.security.web.access.channel.ChannelProcessingFilter;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
//
///**
// *
// * @author stage_IT6
// */
////@EnableGlobalMethodSecurity(prePostEnabled = true)
//@Configuration
//@EnableWebSecurity
//public class SecurityJWTconfig extends WebSecurityConfigurerAdapter{
////    @Autowired
////    private JwtauthenticationProvider AuthProv;
////    @Autowired
////    private JwtAuthenticationEntryPoint entryPoint;
//    
////    @Override
////    public void configure(WebSecurity web) throws Exception {
////        // Filters will not get executed for the resources
////        web.ignoring().antMatchers("/", "/resources/**", "/static/**",
////            "/*.html", "/**/*.html" ,"/**/*.css","/**/*.js","/**/*.png",
////            "/**/*.jpg", "/**/*.gif", "/**/*.svg", "/**/*.ico", "/**/*.ttf",
////            "/**/*.woff","/**/*.otf","/**/*.eot");
////    }
////    @Bean
////    public AuthenticationManager AuthManager(){
////
////        return new ProviderManager(Collections.singletonList(AuthProv));
////    }
////    
////    @Bean
////    public JwtauthenticationTokenFilter authenticationTokenFilter(){
////        JwtauthenticationTokenFilter filter = new JwtauthenticationTokenFilter();
////        filter.setAuthenticationManager(AuthManager());
////        filter.setAuthenticationSuccessHandler(new JwtSuccessHandler());
////        return filter;
////    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//        .exceptionHandling().and()
//        .anonymous().and()
//        // Disable Cross site references
//        .csrf().disable()
//        // Add CORS Filter
//        .addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class)
//        .authorizeRequests()
//                .antMatchers("/**").permitAll()
//                .antMatchers("/API/**").authenticated();
//    }
//    
//    
//    
//}
