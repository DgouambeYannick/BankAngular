package com.bankspring.security.identity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;

import com.bankspring.dao.UserRepository;
import com.bankspring.dto.DroitsDTO;
import com.bankspring.dto.RoleDTO;
import com.bankspring.dto.UserDTO;
import com.bankspring.entities.Droits;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import com.bankspring.metier.IBanqueMetier;
import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private IBanqueMetier IB;
    @Autowired
    private UserRepository userRepo;
    private final AccountStatusUserDetailsChecker detailsChecker = new AccountStatusUserDetailsChecker();

    @Override
    public final TokenUser loadUserByUsername(String username) throws UsernameNotFoundException, DisabledException {
        UserDTO userDTO = new UserDTO();
        User use = new User();
        //List<RoleDTO> rdto = new ArrayList<>();
        List<String> rol = new ArrayList<>();
        List<Droits> ALLd1 = new ArrayList<>();
        final User user = userRepo.findOneByname(username).orElseThrow(() -> new UsernameNotFoundException("Utilisateur Introuvable"));

        List<Role> Lr = IB.RoleByUser(user.getUsers_id());
        for (Role role : Lr) {
            ALLd1 = IB.DroitsByRole(role.getRoles_id());
            for (Droits d : ALLd1) {
                if (rol.isEmpty() || !rol.contains(d.getLibelle())) {
                    rol.add(d.getLibelle());
                }
            }
        }
//        for (Role role : Lr) {
//            List<DroitsDTO> ddto = new ArrayList<>();
//            RoleDTO r = new RoleDTO();
//            rol.add(role.getname());
//            ALLd = IB.DroitsByRole(role.getRoles_id());
//            if (ALLd != null) {
//                for (Droits sm : ALLd) {
//                    DroitsDTO d = new DroitsDTO();
//                    d.setDroits_id(sm.getDroits_id());
//                    d.setLibelle(sm.getLibelle());
//                    ddto.add(d);
//                }
//            }
//            r.setRole_id(role.getRoles_id());
//            r.setLibelle(role.getname());
//            r.setDroits(ddto);
//            rdto.add(r);
//        }
//        try {
//            for (Role role : Lr) {
//                rol.add(role.getname());
//                Lsousm = IB.SousmenuByRole(role.getRoles_id());
//                if (Lsousm != null) {
//                    for (Sousmenu sm : Lsousm) {
//                        if (ALLsousm.contains(sm)) {
//                            break;
//                        }else{
//                            ALLsousm.add(sm);
//                            Lm.add(sm.getMenu());
//                        }
//                    }
//                }
//            }
//            for (Menu m : Lm) {  
//                MenuDTO menuDTO = new MenuDTO();                
//                List<SousmenuDTO> Lsousmdto = new ArrayList<>();
//                Lsousm1 = IB.SousmenuByMenu(m.getCodeMenu());
//                for (Sousmenu ls1 : Lsousm1) {
//                    if(ALLsousm.contains(ls1)){
//                        SousmenuDTO smenuDTO = new SousmenuDTO();
//                        smenuDTO.setLibelle(ls1.getLibelle());
//                        smenuDTO.setSousmenu_id(ls1.getSousmenus_id());
//                        Lsousmdto.add(smenuDTO);
//                    }
//                }
//                menuDTO.setLibelle(m.getLibelle());
//                menuDTO.setMenu_id(m.getCodeMenu());
//                menuDTO.setSousmenuUSER(Lsousmdto);
//                mdto.add(menuDTO);
//            }
//        } catch (Exception e) {
//            throw new RuntimeException(e.getMessage());
//        }
//        userDTO.setMenusUSER(mdto);
        // userDTO.setRolesUSER(rdto);
        userDTO.setRoles(rol);
        try {

            //System.out.println("user.getUsers_id() =" + user.getUsers_id());
            use.setUsers_id(user.getUsers_id());
            use.setNomEmploye(user.getNomEmploye());
            use.setname(user.getname());
            use.setPassword(user.getPassword());
            use.setPhotouser(user.getPhotouser());
            use.setenabled(user.getenabled());
            userDTO.setInfosUSER(use);
        } catch (Exception e) {
            System.out.println(e.getMessage() + " " + user.getUsers_id());
        }
        TokenUser currentUser;
        if (user.getenabled() == 1) {
            currentUser = new TokenUser(userDTO);
        } else {
            throw new DisabledException("L\'utilisateur n\'est pas active (utilisateur desactive)");
            //Si l\'activation est en attente, renvoyer un utilisateur désactivé
            //currentUser = new TokenUser(user, false);
        }
        detailsChecker.check(currentUser);
        return currentUser;
    }
}
