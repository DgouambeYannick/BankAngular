package com.bankspring.security.identity;

import com.bankspring.dto.UserDTO;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;

public class TokenUser extends org.springframework.security.core.userdetails.User {
    private UserDTO user;
    //Pour retourner un utilisateur normal 
    public TokenUser(UserDTO user) {
        super(user.getInfosUSER().getname(), user.getInfosUSER().getPassword(), AuthorityUtils.createAuthorityList(user.getRoles().toString()));
        //super(user.getUserName(), user.getPassword(), true, true, true, true,  AuthorityUtils.createAuthorityList(user.getRole().toString()));
        this.user = user;
    }
    
    public UserDTO getUser() {
        return user;
    }

    public String getRole() {
        return user.getRoles().toString();
    }
}
