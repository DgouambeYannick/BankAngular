package com.bankspring.security.identity;

import com.bankspring.dao.UserRepository;
import com.bankspring.dto.UserDTO;
import com.bankspring.entities.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.*;
import javax.servlet.http.*;
import java.util.*;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


@Service
public class TokenUtil {
    @Autowired
    private UserRepository userRepo;
    //private static final long VALIDITY_TIME_MS = 10 * 24 * 60 * 60 * 1000;// 10 days Validity
    private static final long VALIDITY_TIME_MS =  2 * 60 * 60 * 1000; // 2 hours  validity
    private static final String AUTH_HEADER_NAME = "Authorization";

    private String secret="dgouambe";

    public Optional<Authentication> verifyToken(HttpServletRequest request) {
      final String token = request.getHeader(AUTH_HEADER_NAME);

      if (token != null && !token.isEmpty()){
        final TokenUser user = parseUserFromToken(token.replace("Banque","").trim());
        if (user != null) {
            return  Optional.of(new UserAuthentication(user));
        }
      }
      return Optional.empty();

    }

    //Get UserDTO Info from the Token
    public TokenUser parseUserFromToken(String token){
       // List<Role> lr = new ArrayList();
        Claims claims = Jwts.parser()
            .setSigningKey(secret)
            .parseClaimsJws(token)
            .getBody();

        UserDTO user = new UserDTO();
        User use = new User();
        use.setname((String)claims.get("userId"));
         user.setRoles(((List<String>)claims.get("role")));
         final User u = userRepo.findOneByname(use.getname()).orElseThrow(() -> new UsernameNotFoundException("Utilisateur Introuvable"));
         use.setPassword(u.getPassword());
         
        user.setInfosUSER(use);
        if (user.getInfosUSER().getname() != null && user.getRoles() != null) {
            return new TokenUser(user);
        } else {
            return null;
        }
    }

    public String createTokenForUser(TokenUser tokenUser) {
      return createTokenForUser(tokenUser.getUser());
    }

    public String createTokenForUser(UserDTO user) {
      return Jwts.builder()
        .setExpiration(new Date(System.currentTimeMillis() + VALIDITY_TIME_MS))
        .setSubject(user.getInfosUSER().getNomEmploye())
        .claim("userId", user.getInfosUSER().getname())
        .claim("role", user.getRoles())
        .signWith(SignatureAlgorithm.HS256, secret)
        .compact();
    }

}
