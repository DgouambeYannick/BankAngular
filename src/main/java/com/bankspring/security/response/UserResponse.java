package com.bankspring.security.response;

import com.bankspring.entities.User;
import io.swagger.annotations.*;
import lombok.*;
import java.util.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserResponse extends OperationResponse {
    private User data = new User();
    private List<User> users;
}
