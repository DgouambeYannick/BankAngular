/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.security.response;

import com.bankspring.entities.Client;
import com.bankspring.entities.Operation;
import com.bankspring.entities.User;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author DGOUAMBE
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class ListeResponse extends PageResponse{
    private List<Client> clients;
    private List<User> users;
    private List<Operation> operations;
}
