/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.security.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author stage_IT6
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class DashboardResponse extends OperationResponse{
    private Integer nbreCC;
    private Integer nbreCE;
    private Integer nbreRetrait;
    private Integer nbreVersement;
}
