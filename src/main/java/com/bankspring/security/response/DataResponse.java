/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.security.response;

import com.bankspring.entities.Client;
import com.bankspring.entities.Compte;
import com.bankspring.entities.CompteCourant;
import com.bankspring.entities.CompteEpargne;
import com.bankspring.entities.Role;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author DGOUAMBE
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class DataResponse extends OperationResponse{
    private Client client = new Client();
    private CompteCourant comptecourant = new CompteCourant();
    private CompteEpargne compteEpargne = new CompteEpargne();
    private Role role = new Role();
    
    private List<Client> clients;
    
}
