package com.bankspring.security.config;

import org.springframework.security.config.annotation.web.builders.*;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import com.bankspring.security.identity.*;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
@Order(1)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private TokenUtil tokenUtil;

//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        // Les filtres ne seront pas exécutés pour les ressources
//        web.ignoring().antMatchers("/", "/resources/**", "/static/**", "/*.html", "/**/*.html" ,"/**/*.css",
//                "/**/*.js","/**/*.png","/**/*.jpg", "/**/*.gif", "/**/*.svg", "/**/*.ico", "/**/*.ttf","/**/*.woff","/**/*.otf");
//    }

    //Si la sécurité ne fonctionne pas, vérifiez application.properties si elle est définie sur ignorer
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .exceptionHandling().and()
        .anonymous().and()
        // Désactiver les références Cross site
        .csrf().disable()
        // Ajouter le filtre CORS
        .addFilterBefore(new CorsFilter(), ChannelProcessingFilter.class)
        // Authentification basée sur un jeton personnalisé basée sur l'en-tête précédemment donné au client
        .addFilterBefore(new VerifyTokenFilter(tokenUtil), UsernamePasswordAuthenticationFilter.class)
        // Authentification basée sur JSON personnalisée par POST de {"username": "<nom>", "password": "<password>"} qui définit l'en-tête de jeton lors de l'authentification
        .addFilterBefore(new GenerateTokenForUserFilter ("/session", authenticationManager(), tokenUtil), UsernamePasswordAuthenticationFilter.class)
        .authorizeRequests().antMatchers("/api/**").authenticated();
    }

    /*
    * Si vous souhaitez stocker le mot de passe codé dans vos bases de données et authentifier l'utilisateur
    * basé sur un mot de passe encodé puis décommenter la méthode ci-dessous et fournir un encodeur
//    */
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
    }
    
}
