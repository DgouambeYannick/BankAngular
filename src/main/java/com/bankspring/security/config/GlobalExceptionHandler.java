package com.bankspring.security.config;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bankspring.security.response.*;
import static com.bankspring.security.response.OperationResponse.*;

/*
@ControllerAdvice indique à votre application de spring que cette classe effectuera la gestion des exceptions pour votre application.
@RestController en fera un contrôleur et laissera cette classe afficher la réponse.
Utilisez l'annotation @ExceptionHandler pour définir la classe d'exception à attraper. (Une classe de base attrapera toutes les classes héritées et étendues)*/
@ControllerAdvice
@RestController
public class GlobalExceptionHandler {
    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public OperationResponse handleBaseException(DataIntegrityViolationException e){
        OperationResponse resp = new OperationResponse();
        resp.setOperationStatus(ResponseStatusEnum.ERROR);
        resp.setOperationMessage(e.getRootCause().getMessage());
        return resp;
    }

}
