package com.bankspring.security.config;

import org.springframework.security.core.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.authentication.*;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.json.*;
import lombok.extern.slf4j.Slf4j;

import com.bankspring.security.identity.*;
import com.bankspring.security.session.*;
import static com.bankspring.security.response.OperationResponse.*;
import com.fasterxml.jackson.databind.*;


/* Ce filtre est mappé vers / session et essaie de valider le nom d'utilisateur et le mot de passe */
@Slf4j
public class GenerateTokenForUserFilter extends AbstractAuthenticationProcessingFilter {

    private TokenUtil tokenUtil;

    protected GenerateTokenForUserFilter(String urlMapping, AuthenticationManager authenticationManager, TokenUtil tokenUtil) {
        super(new AntPathRequestMatcher(urlMapping));
        setAuthenticationManager(authenticationManager);
        this.tokenUtil = tokenUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException, JSONException {
        try{
            String jsonString = IOUtils.toString(request.getInputStream(), "UTF-8");
            /* using org.json */
            JSONObject userJSON = new JSONObject(jsonString);
            String username = userJSON.getString("username");
            String password = userJSON.getString("password");
            log.info("username:{} and paswword:{} \n", username, password);
            //final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken("demo", "demo");
            final UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);
            return getAuthenticationManager().authenticate(authToken); // Cela portera à la fonction successfulAuthentication ou faliureAuthentication
        }
        catch( JSONException | AuthenticationException e){
            System.out.println(e.getMessage());
            throw new AuthenticationServiceException(e.getMessage());
        }
    }

    @Override
    protected void successfulAuthentication ( HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication authToken) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authToken);
        /*
        JSONObject jsonResp = new JSONObject();
        TokenUser tokenUser = (TokenUser)authToken.getPrincipal();
        String newToken = this.tokenUtil.createTokenForUser(tokenUser);
        jsonResp.put("token",newToken);
        jsonResp.put("firstName",tokenUser.getUser().getFirstName());
        jsonResp.put("lastName",tokenUser.getUser().getLastName());
        jsonResp.put("email",tokenUser.getUser().getEmail());
        jsonResp.put("role",tokenUser.getRole());
        */

        TokenUser tokenUser = (TokenUser)authToken.getPrincipal();
        SessionResponse resp = new SessionResponse();
        SessionItem respItem = new SessionItem();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String tokenString = this.tokenUtil.createTokenForUser(tokenUser);
//        respItem.setFirstName(tokenUser.getUser().getFirstName());
//        respItem.setLastName(tokenUser.getUser().getLastName());
//        respItem.setUserId(tokenUser.getUser().getUserId());
//        respItem.setEmail(tokenUser.getUser().getEmail());
        respItem.setEnabled(tokenUser.getUser().getInfosUSER().getenabled());
        respItem.setNomEmploye(tokenUser.getUser().getInfosUSER().getNomEmploye());
        respItem.setPhoto(tokenUser.getUser().getInfosUSER().getPhotouser());
        respItem.setUserId(tokenUser.getUser().getInfosUSER().getUsers_id());
        respItem.setUsername(tokenUser.getUser().getInfosUSER().getname());
        respItem.setRoles(tokenUser.getUser().getRoles());
        //respItem.setAllroles(tokenUser.getUser().getRolesUSER());
        respItem.setToken(tokenString);

        resp.setOperationStatus(ResponseStatusEnum.SUCCESS);
        resp.setOperationMessage("Succes de connexion");
        resp.setItem(respItem);
        String jsonRespString = ow.writeValueAsString(resp);

        res.setStatus(HttpServletResponse.SC_OK);
        res.getWriter().write(jsonRespString);
        //res.getWriter().write(jsonResp.toString());
        res.getWriter().flush();
        res.getWriter().close();

        // N'appelez pas supper car il contine la chaîne de filtres super.successfulAuthentication (req, res, chain, authResult);
    }
}
