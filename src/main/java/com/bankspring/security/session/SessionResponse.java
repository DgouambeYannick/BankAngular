package com.bankspring.security.session;

import io.swagger.annotations.*;
import lombok.*;
import java.util.*;
import com.bankspring.security.response.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class SessionResponse extends OperationResponse {
  @ApiModelProperty(required = true, value = "")
  private SessionItem item;
}
