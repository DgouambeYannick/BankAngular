package com.bankspring.security.session;

import com.bankspring.dto.DroitsDTO;
import com.bankspring.dto.RoleDTO;
import com.bankspring.dto.UserDTO;
import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import lombok.*;
import java.util.*;
import io.swagger.annotations.ApiModelProperty;

@Data
public class SessionItem {
    private String  token;
//    private String  userId;
//    private String  firstName;
//    private String  lastName;
//    private String  email;
    private List<String> roles;
 //   private User user;
 //   private List<Role> listrole;
    private Long userId;
    private String username;
    private String NomEmploye;
    private Integer enabled;
    private String photo;
   // private List<RoleDTO> allroles;
   // private UserDTO userDTO;
}
