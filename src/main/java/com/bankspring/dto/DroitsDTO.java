/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dto;

/**
 *
 * @author DGOUAMBE
 */
public class DroitsDTO {
    private Long droits_id;
    private String libelle;

    public DroitsDTO() {
    }

    public DroitsDTO(Long droits_id, String libelle) {
        this.droits_id = droits_id;
        this.libelle = libelle;
    }

    public Long getDroits_id() {
        return droits_id;
    }

    public void setDroits_id(Long droits_id) {
        this.droits_id = droits_id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
    
}
