/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dto;

//import com.bankspring.entities.Role;
import com.bankspring.entities.User;
import java.util.List;

/**
 *
 * @author stage_IT6
 */
public class UserDTO {

//    private Long users_id;
//    private String nomEmploye;
//    private Integer enabled;
//    private String name;
//    private String password;
//    private Role role;
    private User infosUSER;
//    private List<Role> rolesUSER;
    private List<String> roles;
//    private List<Sousmenu> sousmenuUSER;
   // private List<RoleDTO> rolesUSER;
     
    public UserDTO() {
    }

    public UserDTO(User infosUSER) {
        this.infosUSER = infosUSER;
    }
    
    public User getInfosUSER() {
        return infosUSER;
    }

    public void setInfosUSER(User infosUSER) {
        this.infosUSER = infosUSER;
    }
//
//    public List<Role> getRolesUSER() {
//        return rolesUSER;
//    }
//
//    public void setRolesUSER(List<Role> rolesUSER) {
//        this.rolesUSER = rolesUSER;
//    }
//
//    public List<Sousmenu> getSousmenuUSER() {
//        return sousmenuUSER;
//    }
//
//    public void setSousmenuUSER(List<Sousmenu> sousmenuUSER) {
//        this.sousmenuUSER = sousmenuUSER;
//    }
    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    
    
//    public Long getUsers_id() {
//        return users_id;
//    }
//
//    public void setUsers_id(Long users_id) {
//        this.users_id = users_id;
//    }
//
//    public String getNomEmploye() {
//        return nomEmploye;
//    }
//
//    public void setNomEmploye(String nomEmploye) {
//        this.nomEmploye = nomEmploye;
//    }
//
//    public Integer getEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(Integer enabled) {
//        this.enabled = enabled;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public Role getRole() {
//        return role;
//    }
//
//    public void setRole(Role role) {
//        this.role = role;
//    }

//    public List<MenuDTO> getMenusUSER() {
//        return menusUSER;
//    }
//
//    public void setMenusUSER(List<MenuDTO> menusUSER) {
//        this.menusUSER = menusUSER;
//    }

//    public List<RoleDTO> getRolesUSER() {
//        return rolesUSER;
//    }
//
//    public void setRolesUSER(List<RoleDTO> rolesUSER) {
//        this.rolesUSER = rolesUSER;
//    }


}
