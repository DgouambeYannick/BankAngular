/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bankspring.dto;

import com.bankspring.entities.Droits;
import java.util.List;

/**
 *
 * @author DGOUAMBE
 */
public class RoleDTO {
    private Long role_id;
    private String libelle;
    private List<DroitsDTO> droits;

    public RoleDTO() {
    }

    public RoleDTO(Long role_id, String libelle) {
        this.role_id = role_id;
        this.libelle = libelle;
    }
    
    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public List<DroitsDTO> getDroits() {
        return droits;
    }

    public void setDroits(List<DroitsDTO> droits) {
        this.droits = droits;
    }

    
}
