package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompteCourant.class)
public abstract class CompteCourant_ extends com.bankspring.entities.Compte_ {

	public static volatile SingularAttribute<CompteCourant, Double> decouvert;

}

