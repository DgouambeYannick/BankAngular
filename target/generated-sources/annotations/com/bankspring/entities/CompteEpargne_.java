package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CompteEpargne.class)
public abstract class CompteEpargne_ extends com.bankspring.entities.Compte_ {

	public static volatile SingularAttribute<CompteEpargne, Double> taux;

}

