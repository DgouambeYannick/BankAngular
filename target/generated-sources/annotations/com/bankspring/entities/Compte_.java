package com.bankspring.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Compte.class)
public abstract class Compte_ {

	public static volatile CollectionAttribute<Compte, Operation> operations;
	public static volatile SingularAttribute<Compte, Date> Datecreate;
	public static volatile SingularAttribute<Compte, Double> solde;
	public static volatile SingularAttribute<Compte, Client> client;
	public static volatile SingularAttribute<Compte, Long> idompte;
	public static volatile SingularAttribute<Compte, String> codeCompte;
	public static volatile SingularAttribute<Compte, User> user;

}

