package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleSousmenuPK.class)
public abstract class RoleSousmenuPK_ {

	public static volatile SingularAttribute<RoleSousmenuPK, Long> sousmenu_id;
	public static volatile SingularAttribute<RoleSousmenuPK, Long> roles_id;

}

