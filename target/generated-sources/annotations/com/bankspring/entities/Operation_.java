package com.bankspring.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Operation.class)
public abstract class Operation_ {

	public static volatile SingularAttribute<Operation, Long> numeroOperation;
	public static volatile SingularAttribute<Operation, Double> montant;
	public static volatile SingularAttribute<Operation, Date> dateOperation;
	public static volatile SingularAttribute<Operation, User> user;
	public static volatile SingularAttribute<Operation, Compte> compte;

}

