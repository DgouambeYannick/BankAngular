package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Sousmenu.class)
public abstract class Sousmenu_ {

	public static volatile SingularAttribute<Sousmenu, Long> sousmenus_id;
	public static volatile SingularAttribute<Sousmenu, Menu> menu;
	public static volatile CollectionAttribute<Sousmenu, RoleSousmenu> sousmenusrole;
	public static volatile SingularAttribute<Sousmenu, String> Libelle;

}

