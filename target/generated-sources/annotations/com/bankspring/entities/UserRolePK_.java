package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserRolePK.class)
public abstract class UserRolePK_ {

	public static volatile SingularAttribute<UserRolePK, Long> roles_id;
	public static volatile SingularAttribute<UserRolePK, Long> users_id;

}

