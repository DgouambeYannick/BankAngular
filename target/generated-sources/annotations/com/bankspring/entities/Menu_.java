package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Menu.class)
public abstract class Menu_ {

	public static volatile SingularAttribute<Menu, Long> codeMenu;
	public static volatile CollectionAttribute<Menu, Sousmenu> sousmenus;
	public static volatile SingularAttribute<Menu, String> Libelle;

}

