package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleSousmenu.class)
public abstract class RoleSousmenu_ {

	public static volatile SingularAttribute<RoleSousmenu, Sousmenu> sousmenu;
	public static volatile SingularAttribute<RoleSousmenu, Role> role;
	public static volatile SingularAttribute<RoleSousmenu, RoleSousmenuPK> roleSousmenuPK;

}

