package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, User> employeSup;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, Long> users_id;
	public static volatile CollectionAttribute<User, UserRole> usersrole;
	public static volatile SingularAttribute<User, String> nomEmploye;
	public static volatile SingularAttribute<User, Integer> enabled;
	public static volatile SingularAttribute<User, String> photouser;

}

