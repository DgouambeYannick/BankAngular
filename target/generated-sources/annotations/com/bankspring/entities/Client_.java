package com.bankspring.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SingularAttribute<Client, String> adresseclient;
	public static volatile SingularAttribute<Client, String> photoclient;
	public static volatile SingularAttribute<Client, String> nomclient;
	public static volatile CollectionAttribute<Client, Compte> Compte;
	public static volatile SingularAttribute<Client, Long> codeclient;

}

